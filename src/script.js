// Library System Management
const libraryMembers = [];
const nationalLibrary = [];
const booksCheckedOut = [];
const checkOutsHistory = [];



// Search a book in the catalogue
const searchButton = document.getElementById("searchButton");
searchButton.onclick = searchBook;
const searchResult = document.getElementById("searchResult")
function searchBook() {
    const titleToSearch = document.getElementById("titleToSearchInput").value;
    searchBookTitle(titleToSearch);
}
function searchBookTitle(titleToSearch) {
    const indexOfBook = nationalLibrary.findIndex((book) => book.title === titleToSearch);

    if (indexOfBook !== -1) {
        searchResult.innerText = `The book "${titleToSearch}" is available in our catalogue. It is stored on the shelf no. ${indexOfBook + 1}.`
    } else {
        searchResult.innerText = `The book "${titleToSearch}" is not currently available in our catalogue.
Please look for another title in our library. Thank you.`
    }
}



//Initial Books in the Catalogue
addBookToCatalogue("To Kill a Mockingbird", "Harper Lee", 1960, 10);
addBookToCatalogue("1984", "George Orwell", 1949, 12);
addBookToCatalogue("The Great Gatsby", "F. Scott Fitzgerald", 1925, 11);
addBookToCatalogue("Pride and Prejudice", "Jane Austen", 1813, 9);
addBookToCatalogue("The Catcher in the Rye", "J.D. Salinger", 1951, 10);
addBookToCatalogue("Moby-Dick", " Herman Melville", 1851, 11);
addBookToCatalogue("To the Lighthouse", "Virginia Woolf", 1927, 12);
addBookToCatalogue("Brave New World", "Alodus Huxley", 1932, 10);
addBookToCatalogue("The Hobbit", "J.R.R Tolkien", 1937, 13);
addBookToCatalogue("War and Peace", "Leo Tolstoy", 1869, 15);



// Add a book in the catalogue
const addBookButton = document.getElementById("addBookButton");
addBookButton.onclick = addBook;
const addBookResult = document.getElementById("addResult");

function addBook() {
    const title = document.getElementById("titleToAddInput").value;
    const author = document.getElementById("authorToAddInput").value;
    const pubYear = document.getElementById("publishingYearToAddInput").value;
    const price = document.getElementById("priceToAddInput").value;
    addBookToCatalogue(title, author, pubYear, price);
    addBookResult.innerText = `The book was succesfully added to the catalogue`;
}

function addBookToCatalogue(title, author, pubYear, price) {
    const book = {
        title,
        author,
        pubYear,
        price,
        checkedOut: false,
        checkOutHistory: 0
    };
    nationalLibrary.push(book);
    
    updateCatalogue();

    return book;
}

function updateCatalogue() {
    const ourCatalogue = document.getElementById("catalogue");
    ourCatalogue.innerHTML = "";

    nationalLibrary.forEach(book => {
        const bookElement = document.createElement("div");
        bookElement.classList.add("p-8", "border-solid", "border-2", "flex", "flex-col", "items-start", "bg-zinc-300", "shadow-2xl", "h-full", "w-full");

        const titleElement = document.createElement("p");
        titleElement.classList.add("text-left");
        titleElement.textContent = `Title: ${book.title}`;
        bookElement.appendChild(titleElement);

        const authorElement = document.createElement("p");
        authorElement.classList.add("text-left");
        authorElement.textContent = `Author: ${book.author}`;
        bookElement.appendChild(authorElement);

        const yearElement = document.createElement("p");
        yearElement.classList.add("text-left");
        yearElement.textContent = `Year: ${book.pubYear}`;
        bookElement.appendChild(yearElement);

        const historyElement  = document.createElement("p");
        historyElement.classList.add("text-left");
        historyElement.textContent = `Chechout History: ${book.checkOutHistory}`
        bookElement.appendChild(historyElement); 
        
        const deleteButton = document.createElement("button")
        deleteButton.type = "button";
        deleteButton.innerText = "Delete";
        deleteButton.classList.add("bg-blue-500", "text-white", "px-4", "py-2", "rounded", "mt-5")
        
        deleteButton.addEventListener("click", () => {
            console.log("button clicked");
            removeBook(book.title);
            updateCatalogue();
        })
        
        bookElement.appendChild(deleteButton);
        ourCatalogue.appendChild(bookElement);
        
    });
}

// Remove a book from the catalogue
function removeBook(titleToRemove) {
    // find the index of the book to remove
    const indexOfBook = nationalLibrary.findIndex((book) => book.title === titleToRemove);
    // remove the book from the library
    if (indexOfBook !== -1) {
        nationalLibrary.splice(indexOfBook, 1);
        // console.log(`The book "${titleToRemove}" was succesfully removed from the library`)  =================> initilize it as a pop-up message
    }
}



// Creat a new member of the library
const addMemberButton = document.getElementById("addMemberButton");
addMemberButton.onclick = newMember;
const addMemberResult = document.getElementById("addMemberResult")

function newMember() {
    const name = document.getElementById("nameNewMemberInput").value;
    const lastName = document.getElementById("lastNameNewMemberInput").value;
    const email = document.getElementById("emailNewMemberInput").value;
    const age = Number(document.getElementById("ageNewMemberInput").value);
    const memberID = Number(document.getElementById("memberIDNewMemberInput").value);

    createLibraryMember(name, lastName, email, age, memberID)
}

function createLibraryMember(name, lastName, email, age, memberID) {
    
    const member = {
        name,
        lastName,
        email,
        age,
        memberID
    };

    libraryMembers.push(member);

    updateMemeberList();

    addMemberResult.innerText = `${member.name + " " + member.lastName} was registered successfully 
    as a member of the National Library`
}

function updateMemeberList() {
    const ourMembers = document.getElementById("members");
    ourMembers.innerHTML = "";

    libraryMembers.forEach(member => {
        const memberElement = document.createElement("div");
        memberElement.classList.add("p-8", "border-solid", "border-2", "flex", "flex-col", "items-start", "bg-zinc-300", "shadow-2xl", "h-full", "w-full");
        memberElement.innerHTML = `<p>Name: ${member.name + " " + member.lastName}</p><p>email: ${member.email}</p><p>Age: ${member.age}</p><p>ID: ${member.memberID}</p>`;
        
        const deleteMember = document.createElement("button");
        deleteMember.type = "button";
        deleteMember.innerText = "Delete"
        deleteMember.classList.add("bg-blue-500", "text-white", "px-4", "py-2", "rounded", "mt-5");
        
        deleteMember.addEventListener("click", () => {
            console.log("button clicked");
            removeMember(member.ID);
            updateMemeberList();
        })
        
        memberElement.appendChild(deleteMember);
        ourMembers.appendChild(memberElement);
    })
}

function removeMember(ID) {
    const indexOfMember = libraryMembers.findIndex((member) => member.ID === ID);
    if (indexOfMember !== -1) {
        libraryMembers.splice(indexOfMember, 1);
    }
}



//Initial members of the National Library
createLibraryMember("Ledjan", "Micani", "ledjanmicani@gmail.com", 37, 111);



// Check out a book
const checkOutButton = document.getElementById("checkOutButton");
checkOutButton.onclick = checkOut;
const checkOutResult = document.getElementById("checkOutResult");

function checkOut() {
    const titleToCheckOut = document.getElementById("titleToCheckOut").value;
    const ID = Number(document.getElementById("member-IDinput").value);
    const name = document.getElementById("member-name").value;
    const last = document.getElementById("member-lastName").value
    const date = document.getElementById("checkout-date").value;
    checkOutBook(titleToCheckOut, ID, name, last, date);
}

function checkOutBook(titleToCheckOut, ID, name, last, date) {
    const checkMember = libraryMembers.findIndex((member) => member.memberID === ID && member.name === name);

    if (checkMember !== -1) {
        const bookToCheckOut = nationalLibrary.find((book) => book.title === titleToCheckOut);

        if (bookToCheckOut && !bookToCheckOut.checkedOut) {
            checkOutResult.innerText = `The book "${titleToCheckOut}" that you requested was successfully checked out to ${name + " " + last}. Please, make sure that the member returns it three weeks from this date: ${date}.`;
            bookToCheckOut.checkedOut = true;
            bookToCheckOut.checkOutHistory ++;
            booksCheckedOut.push({ title: titleToCheckOut, ID, name, last, date });
            checkOutsHistory.push({ title: titleToCheckOut, history: bookToCheckOut.checkOutHistory, ID, name, last, date, })
        } else if (bookToCheckOut && bookToCheckOut.checkedOut) {
            checkOutResult.innerText = `The book "${titleToCheckOut}" that you requested is not currently available to check out. Please, request another book. Thank you.`;
        } else {
            checkOutResult.innerText = `The book "${titleToCheckOut}" that you requested is not currently available in the library. Please, request another book. Thank you.`;
        }
    } else {
        checkOutResult.innerText = `This reader is not a member of The National Library. Please, register the reader before requesting a book.`;
    }

    updateCheckOuts();
}

function updateCheckOuts() {
    const checkOuts = document.getElementById("check-outs");
    checkOuts.innerHTML = "";

    booksCheckedOut.forEach(checkOut => {
        const checkOutElement = document.createElement("div");
        const returnButton = document.createElement("button");

        returnButton.type = "button";
        returnButton.id = "returnBook";
        returnButton.innerText = "Return";
        returnButton.classList.add("bg-blue-500", "text-white", "px-4", "py-2", "rounded", "mt-5")

        checkOutElement.classList.add("p-8", "border-solid", "border-2", "flex", "flex-col", "items-start", "bg-zinc-300", "shadow-2xl", "h-full", "w-full");
        checkOutElement.innerHTML = `<p>Title: ${checkOut.title}</p><p>ID: ${checkOut.ID}</p><p>Name: ${checkOut.name + " " + checkOut.last}</p><p>Check-out date: ${checkOut.date}</p>`;
        
        returnButton.addEventListener("click", () => {
            returnCheckedOutBook(checkOut.title);
            updateCheckOuts();
        })
        
        checkOutElement.appendChild(returnButton);
        checkOuts.appendChild(checkOutElement);
    })

    updateCatalogue();
}

// Return Checked Out book
function returnCheckedOutBook(titleToReturn) {
    const bookToReturnIndex = nationalLibrary.findIndex((book) => book.title === titleToReturn);

    if (bookToReturnIndex !== -1) {
        nationalLibrary[bookToReturnIndex].checkedOut = false;

        const indexToRemove = booksCheckedOut.findIndex((book) => book.title === titleToReturn)

        if (indexToRemove !== -1 ) {
            booksCheckedOut.splice(indexToRemove, 1);
            return
        }
    } 
}
